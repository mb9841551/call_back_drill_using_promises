const { createDir, deleteFiles } = require("../fs-problem1"); //Importing the functions from fs-problem1.js file

const noOfFiles = 8;
const dirPath = "../Random";

createDir(dirPath, noOfFiles) //call the function
  .then(() => {
    //used chaining to call the deleteFiles function
    return deleteFiles(dirPath, noOfFiles);
  })
  .catch((err) => {
    //catch function to handle any error in process
    console.error("Error in overall process", err);
  });
