const fs = require("fs").promises; //importing fs module using promises
const path = require("path"); //importing path

function createDir(dirPath, noOfFiles) {
  // Function to create a directory and add specified number of files
  return fs
    .mkdir(dirPath) //return the promise
    .then(() => {
      //if promise is resolved then
      let count = 0;

      const writePromises = []; //an empty array which will store writeFile promises

      for (let index = 0; index < noOfFiles; index++) {
        // Loop to create files and write random data to them
        const fileName = `file${index}.json`;
        const fileDestination = path.join(dirPath, fileName); // Join the path of directory to file
        const data = {
          data: Math.floor(Math.random() * 100), //assign random data
        };

        const writePromise = fs
          .writeFile(fileDestination, JSON.stringify(data)) // Write data to the file using promises
          .then(() => {
            count++;
            if (count === noOfFiles) {
              // to check if all files are created or not before passing it to the callback
              console.log(
                "Successfully created the directory and added the files inside the directory"
              );
            }
          })
          .catch(() => {
            console.error("Error writing files");
          });

        writePromises.push(writePromise); //push all the promises into array
      }

      return Promise.all(writePromises); //return the promises object after Promise.all operation on an array of promises
    })
    .catch((err) => {
      //catch function
      console.error("Error making directory", err);
      throw err;
    });
}

function deleteFiles(dirPath, noOfFiles) {
  // Function to delete specified number of files from a directory
  setTimeout(() => {
    return fs
      .readdir(dirPath)
      .then((files) => {
        const deletePromises = [];

        let count = noOfFiles;
        files.forEach((file) => {
          // Loop through files and delete each one using unlink after joining the path
          const fileDestination = path.join(dirPath, file);
          const deletePromise = fs
            .unlink(fileDestination)
            .then(() => {
              count--;
              if (count === 0) {
                //to check if all the fileas are deleted
                console.log("Successfully deleted all files");
              }
            })
            .catch((err) => {
              console.error("Error in deleting files", err);
            });

          deletePromises.push(deletePromise); //push all the promises from unlink to deletePromise array
        });

        return Promise.all(deletePromises); //return the promises object after Promise.all operation on an array of promises
      })
      .catch((err) => {
        //catch function
        console.error("Error in reading files which are to be deleted", err);
        throw err;
      });
  }, 5000);
}

module.exports = {
  //Exporting the functions in an object
  createDir: createDir,
  deleteFiles: deleteFiles,
};
