const fs = require("fs").promises;

function paragraph(path) {
  //Query 1
  return (
    fs
      .readFile(path, "utf-8") // Read the content of the lipsum file
      //Query 2
      .then((data) => {
        //used chaining because i have returned the previous operation and used "then"
        const inUpperCase = data.toUpperCase(); // Convert the data to uppercase
        return fs.writeFile("upperCase.txt", inUpperCase, "utf-8"); // Write the uppercase data to a new file (upperCase.txt)
      })
      .then(() => {
        return fs.appendFile("filenames.txt", "upperCase.txt\n"); // Append the filename to filenames.txt
      })
      //Query 3
      .then(() => {
        return fs.readFile("upperCase.txt", "utf-8"); // Read the content of the uppercase file
      })
      .then((upperCaseData) => {
        const inLowerCase = upperCaseData.toLowerCase(); // Convert the content to lowercase
        const splittedContent = inLowerCase.split(".").filter(Boolean); // Split the content into sentences
        const formattedContent = splittedContent.join("\n"); // Format and join with newlines
        return fs.writeFile("lowerCaseSentences.txt", formattedContent, "utf8"); // Write the formatted content to lowerCaseSentence.txt
      })
      .then(() => {
        return fs.appendFile("filenames.txt", "lowerCaseSentences.txt\n"); // Append the lowerCaseSentence.txt to filenames.txt
      })
      //Query 4
      .then(() => {
        return fs.readFile("lowerCaseSentences.txt", "utf-8"); // Read the content of the lowercase sentence file
      })
      .then((lowerCaseData) => {
        const sortedContent = lowerCaseData.split("\n").sort().join("\n"); // Split the content into lines, sort, and join with newlines
        return fs.writeFile("sortedData.txt", sortedContent, "utf-8"); //Write sortedContent into "sortedData.txt"
      })
      .then(() => {
        return fs.appendFile("filenames.txt", "sortedData.txt\n"); // Append the "sortedData.txt" to filenames.txt
      })
      //Query 5
      .then(() => {
        return fs.readFile("filenames.txt", "utf-8"); // Read the content of the filenames.txt file
      })
      .then((fileNamesData) => {
        const files = fileNamesData.split("\n").filter(Boolean); //split the filenames by using separator ("\n")

        const unlinkFiles = files.map((file) => {
          //used map here because it will give us the new array
          return fs.unlink(file); //delete files using fs.unlink
        });
        return Promise.all(unlinkFiles); //used Promise.all to concurrently finish all the promises in array
      })
      .then(() => {
        console.log("Deleted all files");
      })
      .catch((err) => {
        console.error("Error occurred : ", err); //to handle any error
      })
  );
}

module.exports = paragraph; //exported the function
